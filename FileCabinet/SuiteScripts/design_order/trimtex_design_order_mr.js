/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/runtime'],
/**
 * @param {record} record
 * @param {search} search
 */
function(record, search, runtime) {
   
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {
    	
    	var doId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_id_mr'});
    	
    	record.submitFields({
    		type : 'customrecord_sta_tx_do',
    		id : doId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : 'PROCESSING'
    		}
    	});
    	
    	
    	return JSON.parse(runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_matrix_data_mr'}));
    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
    	var valueObj = JSON.parse(context.value);
    	
    	log.debug('map ' + valueObj.fields.parent, valueObj);
    	
    	context.write(valueObj.fields.parent, valueObj);
    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {
    	for(var i = 0; i < context.values.length; i++){
    		
    		var matrixObj = JSON.parse(context.values[i]);
    		
    		var matrixSubRec = record.create({type : matrixObj.type});
    		
    		Object.keys(matrixObj.fields).forEach(function(field){
    			matrixSubRec.setValue(field, matrixObj.fields[field]);
    		});
    		
    		//Set value for online price
    		/*matrixSubRec.selectLine('price' + matrixObj.price.currency, 
    				matrixSubRec.findSublistLineWithValue('price' + matrixObj.price.currency, 'pricelevel', 5));
    		matrixSubRec.setCurrentSublistValue('price' + matrixObj.price.currency, 'price_1_', matrixObj.price.rate);
    		matrixSubRec.setCurrentSublistValue('price' + matrixObj.price.currency, 'quantity', 0);
    		matrixSubRec.commitLine('price' + matrixObj.price.currency);*/
    		
    		matrixSubRec.setSublistValue('price' + matrixObj.price.currency, 'price_1_', 
    				matrixSubRec.findSublistLineWithValue('price' + matrixObj.price.currency, 'pricelevel', 5), matrixObj.price.rate)
    		
    		
			var matrixSubRecId = matrixSubRec.save();
    		
    		record.submitFields({
        		type : 'customrecord_sta_tx_itemdesign',
        		id : matrixObj.design.id,
        		values : {
        			custrecord_sta_tx_itemdesign_matrix : true
        		}
        	});
    		
			log.debug('reduce ' + matrixSubRecId, matrixObj);
			context.write(matrixSubRecId, matrixObj);
    		
    	}
    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {
    	
    	var doId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_id_mr'});
    	summary.output.iterator().each(function(key, value){
    		
    		var matrixObj = JSON.parse(value);
    		var designMatrixItemLink = record.create({type : 'customrecord_sta_tx_dolink'});
				
			designMatrixItemLink.setValue('custrecord_sta_tx_dolink_item', key);
			designMatrixItemLink.setValue('custrecord_sta_tx_dolink_do', doId);
			
			designMatrixItemLink.save();
			return true;
    	});
    	
    	summary.reduceSummary.errors.iterator().each(function(key, error){
    		log.error('ERROR_CREATING_SUBITEM', key + ' : ' + error);
    	});
    	
    	record.submitFields({
    		type : 'customrecord_sta_tx_do',
    		id : doId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : 'COMPLETE'
    		}
    	});
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
